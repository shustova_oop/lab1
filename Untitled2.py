
# coding: utf-8

# In[67]:


import os


# In[68]:


d = {}
alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
vec = []
sum = 0


# In[69]:


for filename in os.listdir("C:\\Users\\1\\Downloads\\train"):
    with open("C:\\Users\\1\\Downloads\\train\\"+filename, 'r', encoding='utf-8') as f:
        data = f.read()
    for i in alphabet:
        cnt = data[:len(data)//3].lower().count(i)
        vec.append(cnt)
        sum+=cnt
    d[filename[:2]] = vec


# In[70]:


print(d)

